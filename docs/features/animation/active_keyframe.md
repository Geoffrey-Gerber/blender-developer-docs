# Animation & Rigging: Active Keyframe

History:

- Introduced in Blender 2.91
- Active Keyframe: [D7737](https://developer.blender.org/D7737)
- Copy (from active) to Selected Keyframes:
  [D7783](https://developer.blender.org/D7783)

## Description

Just like objects and bones, keyframes can be selected. The
last-selected keyframe is considered the Active Keyframe, and is
highlighted in a different color.

The Active Keyframe determines which keyframe properties are shown in
the properties panel. It also allows "Copy to Selected" functionality,
to copy properties from the active keyframe to the other selected ones.

## Functional Requirements

- Each FCurve can have zero or one Active Keyframes.
- The Graph Editor as a whole should only have one active keyframe,
  which is used for responding to property changes in the property
  panel. For this it uses the active keyframe of the *active FCurve*.
