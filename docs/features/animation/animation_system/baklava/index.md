# Layered Animation: Baklava

*Baklava* is the working title of the in-development layered animation system.
This documentation is tracking the design and development work. At the moment of
writing this intro, this documentation is not complete, but merely a start.

!!! INFO "On the name Baklava"
    The name 'Baklava' is unofficial and might change. It was chosen because the
    project introduces a layered animation system, baklava is layered (and
    delicious), and it nicely alphabetically follows its predecessor
    [Animato](../animato/index.md).


Relevant links:

- [2022: Character Animation workshop](../../module/character_animation_workshop_2022.md)
- [2023: Layered Animation workshop][workshop-2023] (code.blender.org)
- [Layered Animation: Open Questions](https://hackmd.io/@anim-rigging/SkNZ9pLKp) (on HackMD)

[workshop-2023]: https://code.blender.org/2023/07/animation-workshop-june-2023/

## Main Goals

In short, Baklava introduces a new ID data-block `Animation`, which will replace
the `Action` as the defacto container of animation data.

- Multiple data-blocks can be animated by a single `Animation` (and still have different animation).
- Animation can be stored as layers.

## Design

For the broad design, see the [2023 workshop][workshop-2023] blog post. Since
then some things were changed / detailed, which are described here.

### No More Strips

The initial version of Baklava will not have strips. Animation is stored
directly on the layer. This makes the whole UI/UX design a lot simpler, as each
layer will simply have a well-defined value for each property at each point in
time.

Strips will likely be introduced later, in order to be able to replace the NLA.
However, the initial version of Baklava will have the animation data stored
directly on the layer. This simplification will help to release *a* version
earlier, to get it in the hands of animators for feedback.

### Channel Groups & F-Curve Ordering

`Action`s are separated into 'action groups'. These are arbitrarily named
groupings of F-Curves. This has certain downsides:

- **Grouping:** Some tooling assumes that these are *not* arbitrarily named, and
  makes the assumption that if they match a bone name, all the animation for
  that bone is inside that group. These assumptions can fail, as it's possible
  to arbitrarily rearrange F-Curves and even pop them out of a group, or group
  them manually.
- **F-Curve Order:** F-Curves can be manually reordered within the Action. This
  makes it rather complex for the Blender code to efficiently, say, get all
  rotation components, as this requires a full scan of all the F-Curves.

Proposed design for F-Curves in an `Animation` data-block:

**Grouping** would (for now) be hard-coded and automatically managed by Blender.
These groups would have to exist in DNA in order to store things like
expanded/collapsed state, selection state, and color (for example for bones). It
shall *not* be possible to manually add groups or to move F-Curves between
groups.

Blender would create the following groupings:
  - Property groups: "Transforms", "Custom Properties", maybe some other
    classification of properties.
  - Sub-data groups: Bone name, maybe other sub-data as well.

**F-Curve Order:** F-Curves should be ordered by group, and within that group by
RNA path and array index. This makes it predictable (for both animators and
Blender itself) where they are and how they're ordered. For example, the current
Euler Filter operator assumes that F-Curves for rotation components are
sequentially stored. In practice this is true most of the time, but not always.
Enforcing a fixed ordering will make these operations faster and more
predictable. It is also unclear how much advantage manually reorderable F-Curves
bring to animators.

For groupings of sub-data, the group order should follow the sub-data order. For
bones this would simply be alphabetical, but for shapekeys it should follow the
shapekey order. It is unclear whether this reordering would always happen when
reordering the sub-data, or whether it would be some button to press to sync up
the ordering. Probably the former is more desirable.

**Ordering of Groups** manually should likely still be possible. That way an
animator can, for example, order bones in a sensible-for-the-rig way. A common
example is to order the fingers in actual finger order, instead of
alphabetically.
