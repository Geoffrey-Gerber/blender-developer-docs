# IDs

IDs (also known as data-blocks, especially at user level) are the main
core data structure in Blender. Almost any persistent data is an ID, or
belongs to an ID, even mostly UI-related data like the `WindowManager`
or `Screen` ID types.

> WARNING: **Disclaimer**
>
> A lot of those pages are still stubs and need to be fully written,
reviewed, etc.


## ID Type

- **[ID, ID_Type,
  IDTypeInfo](id_type.md)**: Basic
  structure of an ID, and the ID type system.

## ID Runtime Storage

- **[Main data-base](main.md)**: Runtime
  storage of persistent IDs.
- **[Runtime-only IDs](runtime.md)**: The
  various kinds of temporary, runtime-only ID datablocks.
- **[Embedded IDs](embedded.md)**: Some
  IDs (Collections, NodeTrees) can be 'embedded' into other IDs.

## ID Management

- **[ID Management](management/index.md)**:
  Basics over ID creation, copying/duplication, and freeing.
- **[Relationships Between
  IDs](management/relationships.md)**:
  Relationships between different IDs, usage refcounting, remapping...
- **[File
  Paths](management/file_paths.md)**:
  File paths handling in IDs.

## Related Topics

*TODO: Also needs links to related topics: read/write, link/append,
liboverride, animation, DNA, RNA, etc.*
