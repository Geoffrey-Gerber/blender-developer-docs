# DNA

The `.h` files in `source/blender/makesdna/` define the structs, enums,
etc., for all the data that needs to be persisted to disk. These are
usually used in memory in the running system too. For example,
`DNA_mesh_types.h` defines the `Mesh` struct that has enough information
to store a user's mesh on disk. While running, there may be a `Mesh` in
memory, but there will likely be various derived mesh structures that
are more efficient to use during editing, say.

Another aspect of DNA is that it is also a system for helping
compatibility between different versions of Blender. The way this works
is that a coded description of the layout of each struct is also written
to and read from disk. This means that the layout does not have to stay
fixed. The C++ files in the `makesdna` directory are for generating and
interpreting the structure descriptions.

## Renaming

To be written.

## Defaults

To be written.
