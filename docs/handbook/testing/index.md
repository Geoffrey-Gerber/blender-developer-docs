# Automated Tests

Blender uses the CMake testing framework to run Python and GTest tests.
These are run both by developers while working, and on the buildbot to
find problems in pull requests and commits.

Only part of Blender's code base is covered, developers are highly
encouraged to add more tests.
