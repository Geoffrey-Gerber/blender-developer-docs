# Render Tests

Render, compositing and video sequencer tests compare render results
against reference images.

## Inspection

Tests result can be inspected visually through a HTML file that shows
both the new image, reference image and a diff. This can be found at:

``` text
~/blender-git/build/tests/report.html
```

## Adding or Updating Tests

When adding a new test, or when there are benign test failures due to
intentional changes, the reference images can be created or updated as
follows:

``` bash
cd ~/blender-git/build
# Update reference images for cycles tests
BLENDER_TEST_UPDATE=1 ctest -R cycles
```

After this all the tests should pass, and .blend files and references
images can be committed.

## GPU

### Cycles

For testing Cycles GPU rendering, `CYCLES_TEST_DEVICES` in the CMake
configuration is a list of devices to test. By default it is only
`CPU`, but `CUDA`, `OPTIX`, `ONEAPI`, `HIP` and `METAL` can be added.

### EEVEE and Workbench

`WITH_GPU_RENDER_TESTS` enables EEVEE and Workbench tests, which
are currently disabled by default due to different outputs of different
GPUs. They render the same .blend files as Cycles.

### Compositor

By default only CPU compositing is tested, but the GPU backend can
also be tested by enabling `WITH_COMPOSITOR_REALTIME_TESTS`.

## Batching

By default multiple tests are run with a single Blender launch, which
speeds up test execution.

For investigating threading or memory corruption issues, disabling this
can help avoid interference between tests. This is done by turning off
`WITH_TESTS_BATCHED`.