# Forums

## Developer Forum

The official development discussion platform is [devtalk](https://devtalk.blender.org/).

This forum is used for developers to discuss technical topics, get user feedback
on features under development, and for new developers to get involved.

Contributions to translation and documentation can also be discussed. It's all about
collaborating to improve Blender.

Given the much larger number of users than developers and to avoid drowning out
that discussion, features requests and help using Blender is off topic. For these
see [Feature Requests](user_feedback.md) and
[User Communities](https://www.blender.org/community/) instead.

## Announcements

Active developers are encouraged to subscribe to the
[Announcements](https://devtalk.blender.org/c/announcements/27)
category on devtalk for important information.

## Mailing Lists

Until 2023, mailing lists were used to coordinate development, and were
progressively replaced with the DevTalk forum. The lists archives are
available at [archive.blender.org](https://archive.blender.org/lists/).
