# Blender 4.0: Corrective Releases

## Blender 4.0.2

Released on December 5th 2023, Blender 4.0.2 features 41 bug fixes:

- Adjust Real Snow addon to Principled BSDF changes
  blender/blender-addons#105040
- Blender 4.0 crashes when trying to render scene strips
  blender/blender#114982
- Boolean behaves as if the object is on it's origin
  blender/blender#115715
- BSurfaces: 'ToolSettings' object has no attribute 'use_snap_project'
  blender/blender-addons#105026
- Compose key sequences includes continuation keys on Wayland
  blender/blender#114609
- Crash deleting tool node group from outliner
  blender/blender#115037
- Crash opening files with particle boid goal objects
  blender/blender#115471
- Crash undoing painting on linked image from 3DView
  blender/blender#115140
- Crash when creating transition from last reiming key
  blender/blender#115007
- Crash when using snap to volume and clipping region
  blender/blender#115570
- Cycles: Avoid long wait times of compile-gpu step
  blender/blender@fe904e9ff9
- Draw: Buffers never shrink
  blender/blender#114742
- Edge Crease Doesn't Transfer
  blender/blender#115105
- Error when trying to create texture with VDM Brush Baker
  blender/blender-addons#105028
- FBX IO: Fix import of shape key animations without any keyframes
  blender/blender-addons@db7a31b51f
- File selector shrinks on each display under
  KDEblender/blender#113059
- Fix "make doc_man" not working
  blender/blender@175991df4f
- Fix Blender 4.01 crash "EXCEPTION_ACCESS_VIOLATION"
  blender/blender#115144
- Fix build error on architectures without SSE or sse2neon
  blender/blender@641b7808f2
- Fix rare crashes when reading files with multi-levels of libraries.
  blender/blender@6f625cedf1
- Fix shape_key_remove operator leaves shared normals
  blender/blender#115572
- Fix VSE crash when editing retiming
  blender/blender@4d0de9022f
- FModifier.type enum 'NULL' item was renamed to 'nullptr'
  blender/blender#115279
- ID property's id_type enum is missing the SCREEN type
  blender/blender#115151
- Image Editor - Fill Tool doesn't work as expected
  blender/blender#114963
- Increase thread stack size for musl libc
  blender/blender@a33d2ce7bf
- Keyboard layout sometimes ignored on Wayland
  blender/blender#115160
- Man page generation fails with Python error
  blender/blender@90d4364b35
- Man-page fails to build with non-portable
  installblender/blender#115056
- Missing normals on first chunk of array modifier
  blender/blender#115526
- OpenGL: Mark Legacy Intel Drivers Unsupported
  blender/blender#115228
- PyAPI: call 'sys.excepthook' for text editor exceptions
  blender/blender#115090
- RMB Select sometimes to enter pose mode when selected
  blender/blender#115181
- Scrollbars for template_lists in popups update issue
  blender/blender#115363
- Sheen renders incorrectly when viewed head on
  blender/blender#115206
- Snap Base in Camera View crashes Blender
  blender/blender#115153
- Snap to face nearest failing with transformed objects
  blender/blender#114596
- Transform operations not working in particle edit mode
  blender/blender#115025
- VSE offset drawing not working
  blender/blender@6d324dbaec
- Workbench: Negative scaled sculpt mesh artifacts
  blender/blender#114918
- Zero-sized curve leads to OptiX error
  blender/blender#113325

## Blender 4.0.1

Released on November 17th 2023, Blender 4.0.1 features 11 bug fixes:

- Crash opening old files with text objects
  blender/blender#114892
- Crash when adding specular texture slot in texture paint mode
  blender/blender#114848
- Cycles: MetalRT compilation errors on base M3 MacBook Pros with
  factory installed OS
  blender/blender#114919
- glTF: Fix regression in image quality option
  blender/blender-addons@e5ad2e2c16
- Incorrect display of Rec.1886 and Rec. 2020 view transforms
  blender/blender#114661
- Node Wrangler: Principled Textures Setup emissive texture
  blender/blender-addons#104999
- Sculpt Multires drawing broken with no mask
  blender/blender#114841
- Sequencer: bpy.ops.sequencer.split ignores passed frame
  blender/blender#114891
- User Interface: Keep text field empty for global search
  blender/blender#114758
- Windows: Crash Loading Win32 Quick Access
  blender/blender#114855
- Windows: Invalid reuse of shell_link
  blender/blender#114906
