# Add-ons

## Image import as Plane

- Update improving the UI and adding several common material settings
  ([D15744](http://developer.blender.org/D15744),
  blender/blender-addons@2ec6aba72cd3).

## New add-ons

- New Storypencil add-on for Storyboarding in addons-contrib.
  ([T100665](http://developer.blender.org/T100665))

## glTF 2.0

### Importer

- Fix import SK when there is no target on each primitives
  (blender/blender-addons@70ee1a5b6600)
- Code cleanup
  (blender/blender-addons@b9c0e2878e91)
- Add hook before glTF import
  (blender/blender-addons@a1706bd0f04b)
- Tweaks vertex color import
  (blender/blender-addons@cd2d9df92577)
- Fix ortho camera frame import/export
  (blender/blender-addons@8a2443844daf)
- Import/Export lights using correct units
  (blender/blender-addons@9d903a93f03b)

### Exporter

- Reset pose bone between each action
  (blender/blender-addons@e77b55e45a2a)
- Option to export Active Collection only / with or without nested
  (blender/blender-addons@90732dddff7e,
  blender/blender-addons@712f007c7179)
- Big refactoring of primitive extraction, manage custom attributes
  (blender/blender-addons@51e15a9db4ce,
  blender/blender-addons@33e87824dc33,
  blender/blender-addons@7e2fa377ab1f,
  blender/blender-addons@4be7119ac5a7)
- Fix object parented to bone when using rest pose
  (blender/blender-addons@4cb6ebd8747e,
  blender/blender-addons@106cb51ab9ad,
  blender/blender-addons@eb07144ea7e8)
- Manage delta transforms animations
  (blender/blender-addons@58db76bcc6ae,
  blender/blender-addons@726d08c9036b)
- Avoid crash when using apply modifiers + shapekeys
  (blender/blender-addons@bb77e697e13e)
- Fix scene evaluation
  (blender/blender-addons@3c19be6ffa06)
- Fix resolve UVMap
  (blender/blender-addons@bccd6c669db6)
- Better skin checks
  (blender/blender-addons@97bb515d3ac4)
- Clamp base color factor to \[0,1\]
  (blender/blender-addons@e890169e0a62)
- Various hooks & fixes for animation
  (blender/blender-addons@eb3dcfc70c7f)
- Avoid adding multiple neutral bone on same armature
  (blender/blender-addons@846679918718)
- Fix TRS when parent is skined
  (blender/blender-addons@83290c67a63b)
- Fix ortho camera frame import/export
  (blender/blender-addons@8a2443844daf)
- Import/Export lights using correct units
  (blender/blender-addons@9d903a93f03b)
- Non active action objects TRS is now restored at end of actions
  (blender/blender-addons@5dbcd4a3889f)
- When user don't export SK, driver corrective SK animations must not be
  exported
  (blender/blender-addons@c4dddb88e7a7)
- Do not export special attributes, used internally by Blender
  (blender/blender-addons@addeb64f82f4)
- Export SK on no modifiers objects even if 'apply modifier' is on
  (blender/blender-addons@896ee4698f25)
- Fix Traceback in Variant UI
  (blender/blender-addons@61ff4faf8c7d)
- Fix exporting skined mesh without armature
  (blender/blender-addons@0b4844ede970)
- Fix crash with weird missing datablock node group
  (blender/blender-addons@b665525d03ce)
