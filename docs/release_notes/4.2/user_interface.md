# Blender 4.2: User Interface

### Unused IDs Purge UI Improvements

The purge operation now pops-up a dialog where user can choose which options to apply, and get instant feedback on the amounts of unused data-blocks will be deleted.

![New Purge UI popup](ui_new_purge_popup.png)

blender/blender!117304, blender/blender@0fd8f29e88