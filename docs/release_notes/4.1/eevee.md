# Blender 4.1: EEVEE & Viewport

## Light probes

Probe types have been renamed:

* Reflection Cubemap -> Sphere
* Reflection Plane -> Plane
* Irradiance Grid -> Volume
