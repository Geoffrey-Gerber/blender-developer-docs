# Blender 3.0: Pipeline, Assets & IO

## Packing

- Packing linked libraries is now accessible via the File menu
  (blender/blender@ee51e7335552).
- Cleanup of the other External Data options
  (blender/blender@ee51e7335552).

![](../../images/External_Data_Packed.png)

## Alembic

- Animated UV maps are now exported to Alembic
  (blender/blender@3e77f747c0d1).
- Generated mesh vertex coordinates (also known as ORCOs) are now
  exported to and imported from Alembic
  (blender/blender@f9567f6c63e7).
  They're stored in `.arbGeomParams/Pref` in the Alembic file.
- Per vertex UV maps are now imported from Alembic
  (blender/blender@3385c04598f).
  Such UV maps can be defined by other software to reduce file size when
  the mesh is split according to UV islands. Blender, however, still
  stores the UV data per face corner.
- Non functional "Renderable Objects only" option has been removed
  (blender/blender@834e87af7bbf),
  has been superseeded by the following:
- New option to set evaluation mode to Render or Viewport (for
  visibility, modifiers) in
  blender/blender@8f5a4a245388
- New option to always add a cache reader when importing files
  (blender/blender@5b97c00e9fc4e).
  This simplifies workflows for updating objects after imports if they
  change in the cache.

## USD Importer

USD files can now be imported into Blender
(blender/blender@ea54cbe1b42e).
The USD importer works in a similar fashion as the Alembic importer.

Learn more about Importing USD Files in the [Blender
manual](https://docs.blender.org/manual/en/3.0/files/import_export/usd.html#importing-usd-files).

## glTF 2.0

### Importer

- Import custom properties from default scene
  (blender/blender-addons@260ca332f88f643a302bcabf140e3c471c8c621b)
- Fix issue involving flipped bone Z dir
  (blender/blender-addons@b986a52051d48c2cda5a38159d05fb198cc15db7)
- Avoid traceback when trying to import some invalid glTF 2.0 files
  (blender/blender-addons@4d562682c099ce740d705893008a91a30a206710)

### Exporter

- Add option to keep original texture files
  (blender/blender-addons@0cdaac6f9a3e318b1d5db04ade2838d004cd500d)
- Do not export glTF internal settings as extras
  (blender/blender-addons@57ef445314fef1290b155cb182ee7e513bc22008)
- Remove some channel animation if bone is not animated
  (blender/blender-addons@1757ec91e58a23c6dbd31b763267e1d3f5e40026,
  blender/blender-addons@a8c700d4ebe6a1a767758a0b79775d48d7fbe085,
  blender/blender-addons@45132ba0f4410e9b7126bdc0a141ffca8b801c8a)
- Draco: more explicit error message
  (blender/blender-addons@eedbe303799fb8af4e7dafea81a0dd46973fb85b)
- Fix bug when invalid shapekey driver
  (blender/blender-addons@ac6fe2ff7c9b68b8a08e74ac3b8e350a6eb9d24d,
  blender/blender-addons@cd7092335690c5644c5c75cea23672510fd58ce3)
- Fix driver export when shapekey as a dot in name
  (blender/blender-addons@54d8bdf1f15597b9f6e1aaf308b5a0d5df785552)
- Fix default date type at numpy array creation
  (blender/blender-addons@97cf91034d0d5255269a0b8c506f2cdc8e81b789)
- Make sure that addon that changes root gltf are taken into account
  (blender/blender-addons@0aa618c849ffef7b11cef81f1712b89e0b0e337b)
- Don't erase empty dict extras
  (blender/blender-addons@6c1a227b3fa329d56bdaf0e4ebd6d72aa92d2bfe)
- Add merge_animation_extensions_hook
  (blender/blender-addons@669b89d6a56dfebee45c4b72950d212da04d17df)
- Do not export empty node when camera or light export is disable
  (blender/blender-addons@060d0e76fbb2319e12e8fdf8435debe5267bfc7d)
- Fix crash when trying to export some muted driver(s)
  (blender/blender-addons@902b8ba11efa98006b3a247e4549ca69fb458309)
- Change order of format option
  (blender/blender-addons@760b5d3a46416bd55dabec09aa38385ac3ec2467)
- Cleanup: use inverted_safe on matrices
  (blender/blender-addons@29c9aa34754494bc478a08f0be60f38f4844a83d)
- Validate meshes before export
  (blender/blender-addons@5bffedce241f04dc57374ea86f9701e1fddff47f)
- Better 'selection only' management
  (blender/blender-addons@e7f22134350127ac18747c367bb0ad9a1ef2d8a3)
- Fix animation export for objects parented to bones
  (blender/blender-addons@8975ba0a553a6cbe6b9e086a474331bddfb55841)
- Cleanup object animation curves when animation is constant
  (blender/blender-addons@ba969e8b536781450d8b43959c95ca07e886b2b6)
- Change after change on vertex groups data now on mesh
  (blender/blender-addons@64fcec250d2d9450ac64d8314af7092a6922a28b)
- Avoid issue with setting frame with python v \>= 3.10
  (blender/blender-addons@a0d1647839180388805be150794a812c44e59053)
