# Blender 2.92 Release Notes

Blender 2.92 was released on February 25, 2021.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-92/).

## [Geometry Nodes](geometry_nodes.md)

## [User Interface](user_interface.md)

## [Modeling](modeling.md)

## [Sculpt](sculpt.md)

## [Grease Pencil](grease_pencil.md)

## [Cycles](cycles.md)

## [EEVEE](eevee.md)

## [I/O & Overrides](io.md)

## [Python API](python_api.md)

## [Physics](physics.md)

## [Animation & Rigging](animation_rigging.md)

## [Add-ons](add_ons.md)

## [More Features](more_features.md)
